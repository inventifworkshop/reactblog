import React from 'react';
import logo from './assets/images/logo.svg';
import './assets/css/App.css';

//Importar componente
import MiComponente from './components/MiComponente';

//cargamos el componete que se encargará del Routing
import Router from './Router';

//contenido de la página
import Peliculas from './components/Peliculas';


function App() {

  return (
    <div className="App">
      <Router />
    </div>
  );
}

export default App;
