import React, { Component } from 'react';
import { BrowserRouter, Routes, Route, useParams } from 'react-router-dom';


import Peliculas from './components/Peliculas';
import ErrorPageNotFound from './components/ErrorPageNotFound';
import Header from './components/Header';
import Home from './components/Home';
import Footer from './components/Footer';
import Blog from './components/Blog';
import Formulario from './components/Formulario';
// import Search from './components/Search';
import Busqueda from './components/Busqueda';
import Article from './components/Article';
import CreateArticle from './components/CreateArticle';
import UpdateArticle from './components/UpdateArticle';

class Router extends Component {

    render() {
        function GetParamsSearch() {
            let { search } = useParams();
            return (
                <Busqueda
                    search={search}
                />
            );
        }

        function PruebaParametros() {
            let params = useParams();
            let { apellidos } = useParams();

            let siApellidos = (null);

            if (apellidos) {
                siApellidos = (
                    <h2 className='subheader'>Prueba obtener apellidos: {apellidos}</h2>
                );
            }

            return (
                <div id='content'>
                    <h2 className='subheader'>Prueba obtener nombre: {params.nombre}</h2>
                    {siApellidos}
                </div>
            );
        }
        return (
            // configurar rutas y páginas version 6 en adelante de react router dom
            <BrowserRouter>
                <Header />
                <Routes>
                    <Route exact path="/" ClassName={(navData) => navData.isActive ? "active" : "active"} element={<Peliculas />} />
                    <Route exact path="/home" element={<Home />} />
                    <Route exact path="/blog" element={<Blog />} />
                    <Route exact path="/blog/articulo/:id" element={<Article />} />
                    <Route exact path="/blog/crear" element={<CreateArticle />} />
                    <Route exact path="/blog/editar/:id" element={<UpdateArticle />} />
                    <Route exact path="/blog/busqueda/:search" element={<Busqueda />} />
                    {/* <Route exact path="/redirect/:search" element={<GetParamsSearch />} /> */}
                    <Route exact path="/formulario" element={<Formulario />} />
                    <Route exact path="/peliculas" element={<Peliculas />} />
                    <Route path="*" element={<ErrorPageNotFound />} />
                    {/* ruta sin componente */}
                    <Route exact path="/nocomponente" element={<React.Fragment>
                        <h1>Ruta sin componente</h1>
                    </React.Fragment>
                    } />
                    {/* ruta sin componente y con parametros */}
                    <Route exact path="/parametro/:nombre" element={<PruebaParametros />} />
                    <Route exact path="/parametro/:nombre/:apellidos" element={<PruebaParametros />} />
                </Routes>
                <div className='clearfix'></div>
                <Footer />
            </BrowserRouter>
        );
    }
}

export default Router;