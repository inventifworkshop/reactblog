import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import axios from 'axios';
import Global from '../Global';
import Sidebar from './Sidebar';
//importar libreria para fechas
import Moment from 'react-moment';
import 'moment/locale/es';
import { useNavigate } from 'react-router-dom';
function Article() {
    //declarando state
    const [article, setArticle] = useState({});
    const [status, setStatus] = useState(null);

    const { id } = useParams();
    var navigate = useNavigate();
    let url = Global.url;

    useEffect(() => {

        axios.get(url + 'article/' + id)
            .then(res => {
                setArticle(res.data.article)
            })
    }, []);


    function deleteArticle(id){
        axios.delete(url + 'article/' + id)
        .then(res => {
            setStatus("deleted");
            alert('articulo borrado se va  redirigir');
            return navigate("/home");
        })
    }

    function updateArticle(id){
           return navigate("/blog/editar/"+id);
    }

    if(status === 'deleted'){
        console.log('borrado')
    }
    return (
        <div className="center">
            <section id="content">
                <article className="article-item article-detail">
                    <div className="image-wrap">
                        {article.image != null ? (
                            <img src={url + 'get-image/' + article.image} alt={article.title}></img>
                        ) : (
                            <img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQw4VrKw87n9H74GqLn5pbCnUuXuzqIj-K2xQ&usqp=CAU' alt={article.title}></img>
                        )
                        }
                    </div>

                    <h1 className="subheader">{article.title}</h1>
                    <span className='date'>
                        <Moment locale="es" fromNow>{article.date}</Moment>
                    </span>
                    <p>
                        {article.content}
                    </p>
                    {/* se declara una funcion anonima para () flecha para usar la función y mandarle parametros de lo contrario se llama sola constantemente. */}
                    <button className='btn btn-danger' onClick={() => {deleteArticle(article._id)}}>Eliminar </button>
                    <button className='btn btn-warning' onClick={() => {updateArticle(article._id)}}>Editar</button>
                    <div className="clearfix"></div>
                </article>

                {!article &&
                    <h1>No existe</h1>
                }
            </section>
            <Sidebar />
        </div>
    );

}

export default Article;