import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Global from '../Global';
//importar libreria para fechas
import Moment from 'react-moment';
import 'moment/locale/es';
class Articles extends Component {
    url = Global.url;
    state = {
        articles: {},
        status: null,
        flag: null
    }
    componentDidMount() {
        var search = this.props.search
        //si recivo de home true mostraré otra información
        var home = this.props.home;
        if (home === 'true') {
            this.getLastArticles();
        } else if (search && search != null && search != undefined) {
            this.getArticleBySearch(search);
        } else {
            this.getArticles();
        }

    }

    componentDidUpdate(prevProps) {
        if (this.props.search !== prevProps.search) {
            this.getArticleBySearch(this.props.search);
        }
    }

    getArticleBySearch = (search) => {
        axios.get(this.url + "search/" + search)
            .then(res => {
                this.setState({
                    articles: res.data.articles,
                    status: 'success'
                });
            })
            .catch(err => {
                this.setState({
                    articles: [],
                    status: 'success'
                });
            });
    }

    getLastArticles = () => {
        axios.get(this.url + "articles/last")
            .then(res => {
                console.log(res.data)
                this.setState({
                    articles: res.data.articles,
                    status: 'success'
                });
            });
    }
    getArticles = () => {
        axios.get(this.url + "articles")
            .then(res => {
                this.setState({
                    articles: res.data.articles,
                    status: 'success'
                });
            });
    }
    render() {
        return (
            <div id='articles'>
                {this.state.status === 'success' &&
                    <div>
                        {this.state.articles.map((article) => {
                            return (
                                <article key={article._id} className='article-item' id='article-template'>
                                    <div className='image-wrap'>
                                        {article.image != null ? (
                                            <img src={this.url + 'get-image/' + article.image} alt={article.title}></img>
                                        ) : (
                                            <img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQw4VrKw87n9H74GqLn5pbCnUuXuzqIj-K2xQ&usqp=CAU' alt={article.title}></img>
                                        )
                                        }

                                    </div>

                                    <h2 key={article._id}>{article.title}</h2>
                                    <span className='date'>
                                        <Moment locale="es" fromNow>{article.date}</Moment>
                                    </span>
                                    <Link to={'/blog/articulo/' + article._id}>Leer más</Link>
                                    <div className='clearfix'></div>

                                </article>
                            )
                        })}

                    </div>
                }
            </div>
        );
    }


}

export default Articles;