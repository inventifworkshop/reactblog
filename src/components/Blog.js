import React, { Component } from 'react';
import axios from 'axios';
// importando modulos de las paginas
import Slider from './Slider';
import Sidebar from './Sidebar';
import Articles from './Articles';

class Blog extends Component {

    // state = {
    //     articles: {},
    //     status: null
    // }

    // //
    // componentDidMount() {
    //     axios.get("http://localhost:3900/api/articles")
    //         .then(res => {
    //             console.log(res.data)
    //             this.setState({
    //                 articles: res.data.articles,
    //                 status: 'success'
    //             });
    //         });
    // }

    render() {

        return (
            <div id='blog'>
                <Slider
                    title="Blog"
                    size="slider-small"
                />
                <div className="center">
                    <div id="content">
                        {/* Listado de articulos que vendran de la api */}
                        <Articles/>
                    </div>

                    <Sidebar
                        blog="true"
                    />
                </div>
            </div>

        );
    }

}

export default Blog;