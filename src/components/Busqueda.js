import React from 'react';
import { useParams } from "react-router-dom";
// importando modulos de las paginas
import Slider from './Slider';
import Sidebar from './Sidebar';
import Articles from './Articles';


function Busqueda() {
    const { search } = useParams();
    console.log(search)
    return (
        <div id='blog'>
            <Slider
                title={'Busqueda:' + search}
                size="slider-small"
            />
            <div className="center">
                <div id="content">
                    {/* Listado de articulos que vendran de la api */}
                    <Articles
                        search = {search}
                    />
                </div>

                <Sidebar
                    blog="true"
                />
            </div>
        </div>
    );
}

export default Busqueda;