import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useForm } from 'react-hook-form';
//pendiente por validar campos de formulario
//https://react-hook-form.com/api/useform
import axios from 'axios';
import Global from '../Global';

//validacion formularios y alertas

function CreateArticle() {
    var url = Global.url;
    const [article, setArticle] = useState({});
    const [status, setStatus] = useState(null);
    const [selectedFile, setSelectedFile] = useState(null);
    let id;
    var navigate = useNavigate();
    var titleRef = React.createRef();
    var contentRef = React.createRef();

    function saveArticle(e) {
        e.preventDefault();
        // rellenar state con formulario
        axios.post(url + 'save', article)
            .then(res => {
                if (res.data.article) {
                    id = res.data.article._id;
                    console.log('id:' + id);
                    console.log('obj entrando a salvado:' + JSON.stringify(res.data.article));

                    setStatus('waiting');

                    //subir la imagen
                    if (selectedFile !== null) {
                        // sacar el id del articulo;
                        //crear form data y añadir fichero
                        const formData = new FormData();

                        formData.append(
                            'file0',
                            selectedFile,
                            selectedFile.name
                        )

                        // peticion ajax
                        axios.post(url + 'upload-image/' + id, formData)
                            .then(res => {
                                console.log('respuesta de subida de imagen' + JSON.stringify(res));
                                if (res.data.article) {
                                    setStatus('success');

                                    return navigate("/home");
                                } else {
                                    setStatus('fail');
                                }
                            })

                    }

                } else {
                    setStatus('failed');
                    console.log('fallo al crear articulo:' + status)
                    console.log('descrip:' + res)
                }


            }).catch(err => {
                console.log(err);
            });

    }


    function changeState() {
        setArticle({
            title: titleRef.current.value,
            content: contentRef.current.value
        })
    }

    function fileChange(e) {
        setSelectedFile(e.target.files[0]);
        console.log("imagen seleccionada:" + selectedFile);
    }

    return (
        <div className='center'>

            <section id='content'>
                <h1 className='subheader'>Crear Articulo</h1>
                <form className='mid-form' onSubmit={saveArticle}>
                    <div className='form-group'>
                        <label htmlFor='title'>Titulo</label>
                        <input type='text' name="title" ref={titleRef} onChange={changeState} />
                    </div>
                    <div className='form-group'>
                        <label htmlFor='content'>Contenido</label>
                        <textarea name="content" ref={contentRef} onChange={changeState}></textarea>
                    </div>
                    <div className='form-group'>
                        <label htmlFor='file0'>Imagen</label>
                        <input type='file' name="file0" onChange={fileChange} />
                    </div>

                    <input type='submit' value="Guardar" className="btn btn-success" />
                </form>
            </section>
        </div>

    )
}

export default CreateArticle;