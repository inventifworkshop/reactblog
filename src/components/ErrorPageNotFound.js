import React, { Component } from 'react';

const ErrorPageNotFound = () => {
    return (
        <section id="content">
            <h2 className='subheader'>
                Pagina no encontrada
            </h2>
        </section>
    );
}

export default ErrorPageNotFound;