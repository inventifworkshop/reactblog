import React, { Component } from 'react';

class Pelicula extends Component {

    /*     se necesito hacer una funcion extra, para mandarle solo el objeto de la pelicula ya que si se hacia de esta manera
           <button onClick={this.props.pelicula}>
                Marcar favorita
            </button>
            se enviaba todo el objeto complementario
            
    */
    marcar = () => {
        // Se le esta ejecutando una funcion que tiene el padre Pelicula y tambien enviando valores
        this.props.marcarFavorita(this.props.pelicula, this.props.indice)
    }

    render() {
        const pelicula = this.props.pelicula;
        const { titulo, imagen } = this.props.pelicula;
        return (
            <article className="article-item" id="article-template">
                <div className="image-wrap">
                    <img src={imagen} alt={titulo} />
                </div>

                <h2>{titulo}</h2>
                <span className="date">
                    Hace 5 minutos
                </span>
                <a href="#">Leer más</a>
                {/* this.props.marcarFavorita estamos ejecutando la funcion que tiene el padre*/}
                <button onClick={this.marcar}>
                    Marcar favorita
                </button>

                <div className="clearfix"></div>
            </article>
        );
    }

}

export default Pelicula;