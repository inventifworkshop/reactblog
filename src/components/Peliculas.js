import React, { Component } from 'react';
import Pelicula from './Pelicula';
import Slider from './Slider';
import Sidebar from './Sidebar';
class Peliculas extends Component {

    state = {

    }
    //para cargar información antes que se carge el componente
    componentWillMount() {
        this.setState({
            peliculas: [
                {
                    titulo: "Sherk",
                    imagen: "https://es.web.img3.acsta.net/pictures/14/03/06/10/13/369709.jpg"
                },
                {
                    titulo: "Sherk 2",
                    imagen: "https://es.web.img2.acsta.net/medias/nmedia/18/83/06/92/20070785.jpg"
                },
                {
                    titulo: "Sherk 3",
                    imagen: "https://www.ecartelera.com/carteles/1400/1498/001_m.jpg"
                },
                {
                    titulo: "Sherk 4",
                    imagen: "https://http2.mlstatic.com/D_NQ_NP_715966-MLM43197637202_082020-W.jpg"
                },
            ],
            nombre: 'Victor Luna',
            favorita: {}
        }

        )
    }

    // metodo cuando ya se ha cargado el componente
    componentDidMount() {
        console.log("Se ha cargado el componente");
    }

    cambiarTitulo = () => {
        var { peliculas } = this.state;
        peliculas[0].titulo = "Cambio de titulo";

        this.setState({
            peliculas: peliculas
        });
    }

    favorita = (pelicula, indice) => {
        console.log(pelicula, indice);
        this.setState({
            favorita: pelicula
        });

    }
    render() {
        // ejemplo de como pasar estilos personalizados a un div
        var pStyle = {
            background: 'green',
            color: 'white',
            padding: '10px'
        }

        var intervalId = setInterval(function () {
            //console.log("Interval reached every 5s")
            //obtener posición actual de la pieza 
            // pintar en la nueva posición
        }, 5000);
        return (
            <React.Fragment>
                <Slider
                    title="Peliculas"
                    size="slider-small"
                />
                <div className="center">
                    <div id='content'>
                        <h2 className='subheader'>Listado de Peliculas</h2>
                        <p>Seleccionar de las peliculas de {this.state.nombre}</p>
                        <div><button onClick={this.cambiarTitulo}>Cambiar Titulo</button> </div>

                        {/* crearemos una condicion para cuando favorita tenga valor */}
                        {this.state.favorita.titulo ? (
                            <p className='favorita' style={pStyle}>
                                <strong>La pelicula favorita es:</strong>
                                <span> {this.state.favorita.titulo}</span>
                            </p>
                        ) : (<p>No hay peliculas favoritas seleccionada</p>)
                        }

                        <div id='articles'>
                            {
                                this.state.peliculas.map((pelicula, i) => {
                                    return (
                                        <Pelicula
                                            key={i}
                                            pelicula={pelicula}
                                            indice={i}
                                            /* marcarFavorita es nombre que espera recibir en componente hijo que a su vez le estamos pasando la funcion */
                                            marcarFavorita={this.favorita}
                                        />
                                    )
                                })
                            }
                        </div>
                    </div>
                </div>
                <Sidebar
                    blog="false"
                />
            </React.Fragment>
        );
    }
}

export default Peliculas;