import React, { Component } from 'react';
// importando modulos de las paginas
import Slider from './Slider';
import Sidebar from './Sidebar';
import Articles from './Articles';
class Search extends Component {

    render() {
      
        //var busqueda = this.props.match.params.search;
        return (
            <div id='blog'>
                <Slider
                    title='Busqueda'
                    size="slider-small"
                />
                <div className="center">
                    <div id="content">
                        {/* Listado de articulos que vendran de la api */}
                        <Articles />
                    </div>

                    <Sidebar
                        blog="true"
                    />
                </div>
            </div>

        );
    }

}

export default Search;