import React, { useState } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';

function Sidebar(props) {
    //  javascript code
    // Declaración de una variable de estado que llamaremos "inputSerch"
    const [value, setValue] = useState("");
    let navigate = useNavigate();


    function testFunction(e) {
        e.preventDefault();
        return navigate("/blog/busqueda/" + value);
    }



    return (
        <aside id="sidebar">
            {props.blog === "true" &&
                <div id="nav-blog" className="sidebar-item">
                    <h3>Puedes hacer esto</h3>
                    <NavLink className="btn btn-success" to="/blog/crear">Crear Articulo</NavLink>
                </div>
            }

            <div id="search" className="sidebar-item">
                <h3>Buscador</h3>
                <p>Encuentra el artículo que buscas</p>
                <form onSubmit={testFunction}>
                    <input onChange={e => setValue(e.target.value)} type="text" name="search" />
                    <button type="submit" name="submit" value="Buscar" className="btn" >Buscar</button>
                </form>
            </div>
        </aside>
    );
}

export default Sidebar;
