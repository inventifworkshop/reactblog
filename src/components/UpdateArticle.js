import React, { useState, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useForm } from 'react-hook-form';
//pendiente por validar campos de formulario
//https://react-hook-form.com/api/useform
import axios from 'axios';
import Global from '../Global';

//validacion formularios y alertas

function UpdateArticle() {
    var url = Global.url;
    const [article, setArticle] = useState({});
    const [status, setStatus] = useState(null);
    const [selectedFile, setSelectedFile] = useState(null);
    const { id } = useParams();
    var navigate = useNavigate();
    var titleRef = React.createRef();
    var contentRef = React.createRef();

    //cargamos la información en url y se la asignamos a state de article como objeto
    useEffect(() => {
        axios.get(url + 'article/' + id)
            .then(res => {
                setArticle(res.data.article)
            })
    }, []);


    function saveArticle(e) {
        e.preventDefault();
        // rellenar state con formulario
        console.log('entrando a save:'+id)
        axios.put(url + 'artile/'+id, article)
            .then(res => {
                if (res.data.article) {
                    id = res.data.article._id;

                    setStatus('waiting');

                    //subir la imagen
                    if (selectedFile !== null) {
                        // sacar el id del articulo;
                        //crear form data y añadir fichero
                        const formData = new FormData();

                        formData.append(
                            'file0',
                            selectedFile,
                            selectedFile.name
                        )

                        // peticion ajax
                        axios.post(url + 'upload-image/' + id, formData)
                            .then(res => {
                                console.log('respuesta de subida de imagen' + JSON.stringify(res));
                                if (res.data.article) {
                                    setStatus('success');

                                    return navigate("/home");
                                } else {
                                    setStatus('fail');
                                }
                            })

                    }

                } else {
                    setStatus('failed');
                    console.log('fallo al crear articulo:' + status)
                    console.log('descrip:' + res)
                }


            }).catch(err => {
                console.log(err);
            });

    }

    // usamos la funcion para cambiar el estado continguamente a como se va escribiendo
    function changeState() {
        setArticle({
            title: titleRef.current.value,
            content: contentRef.current.value
        })
        console.log(JSON.stringify(article))
    }

    function fileChange(e) {
        setSelectedFile(e.target.files[0]);
        console.log("imagen seleccionada:" + selectedFile);
    }

    return (
        <div className='center'>

            <section id='content'>
                <h1 className='subheader'>Actualizar Articulo</h1>
                <form className='mid-form' onSubmit={saveArticle}>
                    <div className='form-group'>
                        <label htmlFor='title'>Titulo</label>
                        <input value={article.title != null ? (article.title) : ('')} type='text' name="title" ref={titleRef} onChange={changeState} />
                    </div>
                    <div className='form-group'>
                        <label htmlFor='content'>Contenido</label>
                        <textarea name="content" value={article.content != null ? (article.content) : ('')} ref={contentRef} onChange={changeState}></textarea>
                    </div>
                    <div className='form-group'>
                        <label htmlFor='file0'>Imagen</label>
                        <input type='file' name="file0" onChange={fileChange} />
                    </div>

                    <div className="image-wrap">
                        {article.image != null ? (
                            <img src={url + 'get-image/' + article.image} alt={article.title}></img>
                        ) : (
                            <img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQw4VrKw87n9H74GqLn5pbCnUuXuzqIj-K2xQ&usqp=CAU' alt={article.title}></img>
                        )
                        }
                    </div>

                    <input type='submit' value="Guardar" className="btn btn-success" />
                </form>
            </section>
        </div>

    )
}

export default UpdateArticle;


